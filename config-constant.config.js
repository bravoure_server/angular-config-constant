

(function () {
    'use strict';

    angular
        .module('bravoureAngularApp')
        .constant('PATH_CONFIG', {
            'SECTIONS': 'app/sections/',
            'TEMPLATES': 'app/templates/',
            'COMPONENTS': 'app/shared/components/v1.0/',
            'BRAVOURE_COMPONENTS': 'extend/jello_components/',
            'EXTEND_CUSTOM': 'extend/custom_components/',
            'EXTEND': 'extend/jello_components_extended/',
            'EXTEND_BOWER': 'extend/bower/bower_components/',
            'ASSETS': 'extend/assets/',
            'SHARED': 'app/shared/',
            'SHARED_DIRECTIVES': 'app/shared/directives/',
            'SHARED_SERVICES': 'app/shared/services/',
            'THEME_COMPONENTS': 'assets/theme/shared/components/',
            'IMAGES': 'assets/images/',
            'IMAGES_HOST': 'http://t-core-sp.bravoure.nl',
            'HOME_IDENTIFIER': 'top.homepage',
            'ERROR_IDENTIFIER': 'top.error_page'
        })
        .constant('DATA', data);
})();
